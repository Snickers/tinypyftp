import os
import ftplib
import time

# TODO
# Transfer Progress
# Get files by mask


class TinyPyFTP(object):
    def __init__(self, host, login=None, password=None):
        self.host = host
        self.login = login
        self.password = password

        try:
            ftp = ftplib.FTP(self.host)
        except ftplib.error_perm:
            print "Can't reach FTP server at %s" % self.host
            raise SystemExit
        try:
            ftp.login(self.login, self.password)
        except ftplib.error_perm:
            print 'Invalid login or password.'
            raise SystemExit

        print "Connected to %s" % self.host
        self.ftp = ftp
        self.yta = False  # Yes to all
        self.nta = False  # No to all

    def run(self):
        self.fileList()
        while True:
            try:
                cmd = raw_input('%s || %s: ' % (os.getcwd(), self.ftp.pwd()))
                print "=" * 50
                self.action(cmd)
            except KeyboardInterrupt:
                print 'Bye!'
                raise SystemExit
            except Exception as e:
                print "An error has occured: %s" % e
            finally:
                os.chdir(os.path.dirname(os.path.realpath(__file__)))  # Changes the working directory back, in case it was modified by -o param

    def action(self, cmd):
        # Resets the state of Yes to all/No to all switches
        self.yta = False
        self.nta = False
        user_input = cmd.strip().split(' ')
        command = user_input[0]

        if command == 'dir':
            self.ftp.dir()
        elif command == 'get':
            fname = user_input[1]
            if not fname:
                raise Exception('Please specify a name of file to retrieve.')

            self.root = fname
            if '-o' in user_input:
                destination = user_input[3]
                if not destination:
                    raise Exception('Please specify an output path.')
                try:
                    os.chdir(destination)
                except:
                    os.mkdir(destination)
                    os.chdir(destination)

            if self.get(fname):
                print '\nDone'

        elif command == 'send':
            fname = user_input[1]
            if not os.path.exists(os.path.abspath(fname)):
                raise Exception('Invalid file or path.')

            self.root = fname
            if self.send(fname):
                print '\nDone'

        elif command == 'find':
            string = user_input[1]
            self.find(string)

        elif command == 'rename':
            fr = user_input[1]
            to = user_input[2]
            self.rename(fr, to)

        elif command == 'rm':
            name = user_input[1]
            if name in self.ftp.nlst():
                self.rm(name)
            else:
                raise Exception('No such file or directory.')

        elif command == 'cd':
            dirname = user_input[1]
            self.changeDir(dirname)

        elif command == 'sync':
            destination = '>' if '>' in user_input[1:] else '<'
            try:
                br = user_input.index(destination)
                remote = user_input[br + 1] if br + 1 < len(user_input) and user_input[br + 1] != command else None
            except ValueError:
                br = 2
                remote = user_input[br] if br < len(user_input) and user_input[br] != command else None

            local = user_input[br - 1] if br - 1 < len(user_input) and user_input[br - 1] != command else None

            if self.sync(destination, local, remote):
                print '\nDone'

        else:
            raise Exception('Invalid command, try cd, dir, get or send.')        

    def rm(self, name):
        answer = raw_input('Are you sure you want to delete %s? Yes/No.\n' % name)
        if 'yes' in answer:
            try:
                self.ftp.delete(name)
            except ftplib.error_perm:
                raise Exception('Unable to delete file or folder.')
        else:
            return

    def get(self, path):
        try:
            self.ftp.cwd(path)  # if directory
            files = []
            self.ftp.retrlines('LIST', files.append)
            total = files[0].split(" ")[1]
            # Check if the directory is empty. If it is - create an empty dir in local file system
            # and return to parent directory
            if total is '0':
                try:
                    os.mkdir(path)
                except:
                        pass  # local dir already exists
                self.ftp.cwd('..')
            else:
                try:
                    os.chdir(path)
                except:
                    os.mkdir(path)
                    os.chdir(path)

                for index, f in enumerate(self.ftp.nlst()):
                    self.get(f)
                    # directory traversal, hopefully works properly
                    if index == len(self.ftp.nlst()) - 1:
                        self.ftp.cwd('..')
                        os.chdir(os.pardir)

        except ftplib.error_perm:
            try:
                self.handleFileTransfer(path, method='get')
            except:
                try:
                    os.unlink(path)
                except:
                    pass
                print "Unable to download file: %s" % path
                return False

        return True

    def send(self, path):
        try:
            # Folder
            os.chdir(path)
            base = os.path.basename(path)
            if len(os.listdir(os.getcwd())) == 0:
                os.chdir(os.pardir)
                try:
                    self.ftp.cwd(base)
                except:
                    self.ftp.mkd(base)

            else:
                try:
                    self.ftp.cwd(base)
                except:
                    self.ftp.mkd(base)
                    self.ftp.cwd(base)

                for index, entry in enumerate(os.listdir(os.getcwd())):
                    self.send(entry)

                    objs = len(os.listdir(os.getcwd()))
                    if index == objs - 1:
                        self.ftp.cwd('..')
                        os.chdir(os.pardir)

        except IOError as e:
            print e, index, len(os.listdir(os.getcwd())), entry
            return False
        except:
            # File
            self.handleFileTransfer(path, method='send')

        return True

    def sync(self, destination, local=None, remote=None):
        if not local:
            local = os.getcwd()
        else:
            try:
                os.chdir(local)
            except:
                raise Exception('Invalid local directory')

        if not remote:
            remote = self.ftp.pwd()
        else:
            try:
                self.ftp.cwd(remote)
            except ftplib.error_perm:
                raise Exception('Invalid remote path')

        uploaded = []
        localFiles = os.listdir(local)
        try:
            remoteFiles = self.ftp.nlst()
        except ftplib.error_perm:
            remoteFiles = []

        if destination is '>':
            for f in localFiles:
                if f not in remoteFiles:
                    self.send(f)
                    if not os.path.isdir(f):
                        uploaded.append(f)
                else:

                    lmd = time.ctime(os.path.getmtime(f))
                    try:
                        resp, rmd = self.ftp.sendcmd('MDTM %s' % f)
                    except:
                        rmd = lmd

                    if rmd != lmd:
                        self.send(f)
                        uploaded.append(f)
        else:
            for f in remoteFiles:
                # Make sure we didn't just upload the file and it's not in the local filesystem
                if f not in uploaded and f not in localFiles:
                    self.get(f)
                else:
                    if f not in uploaded:
                        self.get(f)

        return True

    def handleFileTransfer(self, files, method='get'):
        try:
            if self.yta:
                raise
            elif self.nta:
                return

            if method is 'get' and not os.path.exists(files) or method is 'send' and files not in self.ftp.nlst():
                raise

            while True:
                answer = raw_input('File %s already exists. Overwrite? Yes/No/YTA/NTA\n' % files).lower()
                if answer in ('yes', 'yta'):
                    if answer == 'yta':
                        self.yta = True
                    raise
                elif answer in('no', 'nta'):
                    if answer == 'nta':
                        self.nta = True
                    return
                else:
                    print 'Please type one of the following: "Yes/No/YTA/NTA"'
        except:
            methods = {
                'send': ['rb', 'STOR', 'Sending', ],
                'get': ['wb', 'RETR', 'Receiving', ],
            }

            tp, mt, string = methods.get(method)
            with open(files, tp) as f:
                if method is 'get':
                    self.ftp.retrbinary('%s %s' % (mt, files), f.write)
                    try:
                        path = self.ftp.pwd().split(self.root)[1]
                    except:
                        path = ''
                else:
                    self.ftp.storbinary('%s %s' % (mt, files), f)
                    try:
                        path = os.getcwd().split(self.root)[1]
                    except:
                        path = ''
                # Filename truncating
                if len(files) > 30:
                    files = files[:25] + '*' + files[-4:]
                print "%s: %s/%s\t\t\t\t\r" % (string, path, files),

    def find(self, string):
        matches = []
        for name in self.ftp.nlst():
            if string in name:
                matches.append(name)

        if matches:
            for m in matches:
                print m
        else:
            print 'No results'

    def fileList(self):
        try:
            for f in self.ftp.nlst():
                print f

            print "=" * 50
        except ftplib.error_perm:
            pass

    def changeDir(self, newdir):
        try:
            self.ftp.cwd(newdir)
            self.fileList()
        except ftplib.error_perm:
            raise Exception('Not a directory or no access.')

    def rename(self, fr, to):
        try:
            self.ftp.rename(fr, to)
            self.fileList()
        except:
            raise Exception('No such file or directory.')
