import argparse
import core

parser = argparse.ArgumentParser()
parser.add_argument('-s', help='FTP server to connect to', metavar='')
parser.add_argument('-u', help='FTP user', metavar='')
parser.add_argument('-p', help='FTP password', metavar='')
args = parser.parse_args()

if __name__ == '__main__':

    tpftp = core.TinyPyFTP(args.s, args.u, args.p)
    tpftp.run()
